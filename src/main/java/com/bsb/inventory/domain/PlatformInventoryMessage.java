package com.bsb.inventory.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlatformInventoryMessage {
    private int code;

    private String key;

    private String message;

    private String parameter;
}
