package com.bsb.inventory.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class InventoryKey {
    private String sku;
    private String brand;
    private String catalogCode;

    public InventoryKey(String sku, String brand, String catalogCode) {
        this.sku = sku;
        this.brand = brand;
        this.catalogCode = catalogCode;
    }
}
