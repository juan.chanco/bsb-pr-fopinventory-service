package com.bsb.inventory.domain;

import com.bsb.fop.wsdl.product.Inventory;
import com.bsb.fop.wsdl.product.ProductInventory;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
// just matching the current order
@JsonPropertyOrder({"Messages"})
public class PlatformInventory {
    @JsonProperty("Messages")
    @Builder.Default
    // not currently being stored. just here from compatibility
    private List<PlatformInventoryMessage> messages = Collections.emptyList();

    private long BackOrderInStockDRD;
    private int BackOrderInStockQuantity;
    private long BackOrderInStockSRD;
    private long BackOrderOutOfStockDRD;
    private int BackOrderOutOfStockQuantity;
    private long BackOrderOutOfStockSRD;
    private int fromCache;
    private long InStockDRD;
    private long InStockDropshipDRD;
    private int InStockDropshipQuantity;
    private long InStockDropshipSRD;
    private long InStockExpressDRD;
    private int InStockQuantity;
    private long InStockSRD;
    private String SKU;
    private String SubstitutionOfferCode;
    private String SubstitutionSkuOrKitCode;
    private String SubstitutionTypeName;
    private Boolean isDropship;
    private Boolean isBackorderRemindMe;

    private ZonedDateTime lastUpdated;

    public static Optional<PlatformInventory> ofInventory(Inventory inventory) {
        return  Optional.ofNullable(inventory.getProduct().getValue()).map(pi ->
                PlatformInventory.ofProductInventory(pi));
        // from old code. don't think we really need to store error Messages
        // they'll be logged elsewhere
        //if (inventory.getMessages() != null
        //&& inventory.getMessages().getMessagesCollection() != null
        //&& CollectionUtils.isNotEmpty(inventory.getMessages().getMessagesCollection().getWebServiceError())) {


        //List<WebServiceError> messageList = inventory.getMessages().getMessagesCollection().getWebServiceError();

        //pi.setMessages(messageList.stream().map(
        //wse ->
        //PlatformInventoryMessage.builder()
        //.code(wse.getErrorCode())
        //.key(wse.getErrorKey())
        //.message(wse.getErrorMessage())
        //.parameter(wse.getParameter())
        //.build()
        //).collect(Collectors.toList()));
        //}
    }

    // helper function to keep the type system from getting confused
    private static PlatformInventory ofProductInventory(ProductInventory product) {
        return PlatformInventory.builder()
            .BackOrderInStockDRD(product.getBackOrderInStockDRD().toGregorianCalendar().getTimeInMillis())
            .BackOrderInStockQuantity(product.getBackOrderInStockQuantity())
            .BackOrderInStockSRD(product.getBackOrderInStockSRD().toGregorianCalendar().getTimeInMillis())
            .BackOrderOutOfStockDRD(product.getBackOrderOutOfStockDRD().toGregorianCalendar().getTimeInMillis())
            .BackOrderOutOfStockQuantity(product.getBackOrderOutOfStockQuantity())
            .BackOrderOutOfStockSRD(product.getBackOrderOutOfStockSRD().toGregorianCalendar().getTimeInMillis())
            .fromCache(product.getFromCache())
            .InStockDRD(product.getInStockDRD().toGregorianCalendar().getTimeInMillis())
            .InStockDropshipDRD(product.getInStockDropshipDRD().toGregorianCalendar().getTimeInMillis())
            .InStockDropshipQuantity(product.getInStockDropshipQuantity())
            .InStockDropshipSRD(product.getInStockDropshipSRD().toGregorianCalendar().getTimeInMillis())
            .InStockExpressDRD(product.getInStockExpressDRD().toGregorianCalendar().getTimeInMillis())
            .InStockQuantity(product.getInStockQuantity())
            .InStockSRD(product.getInStockSRD().toGregorianCalendar().getTimeInMillis())
            .SKU(product.getSKU().getValue())
            .SubstitutionOfferCode(product.getSubstitutionOfferCode().getValue())
            .SubstitutionSkuOrKitCode(product.getSubstitutionSkuOrKitCode().getValue())
            .SubstitutionTypeName(product.getSubstitutionTypeName().getValue())
            .isDropship(product.isIsDropship())
            .isBackorderRemindMe(product.isIsBackorderRemindMe())
            // TODO: remove once set elsewhere
            .lastUpdated(ZonedDateTime.now())
            .build();

    }
}
