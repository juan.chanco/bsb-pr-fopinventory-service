package com.bsb.inventory.dao;

import com.bsb.fop.client.ProductInventoryClient;
import com.bsb.fop.domain.Brand;
import com.bsb.fop.domain.CatalogCode;
import com.bsb.fop.wsdl.product.WebServiceError;
import com.bsb.fop.wsdl.product.GetProductInventoryResponse;
import com.bsb.fop.wsdl.product.Inventory;
import com.bsb.inventory.domain.PlatformInventory;

import org.springframework.stereotype.Service;

import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class FopProductInventoryDao {
    private final ProductInventoryClient productInventoryClient;

    public Optional<PlatformInventory> getInventory(String sku, Brand brand, CatalogCode catalogCode) {
        GetProductInventoryResponse response = productInventoryClient.getProductInventory(sku, brand, catalogCode);

        Inventory inventory = response.getGetProductInventoryResult().getValue();
        if (!inventory.isSuccessFlag()) {
            log.info("FopInventory - Unsuccessful call. {}/{}/{}", sku, brand, catalogCode);
            WebServiceError webServiceError = inventory.getMessages().getValue().getMessagesCollection().getValue().getWebServiceError().stream()
                .findFirst()
                .orElseThrow(() -> {
                    return new RuntimeException("No message in error response");
                });
            if (webServiceError.getErrorCode() == 1381) {
                return Optional.empty();
            } else {
                throw new RuntimeException("webServiceError.getErrorMessage().getValue()");
            }

        }

        return PlatformInventory.ofInventory(inventory);
        //return getProductInventory(sku, brand.toString(), catalogCode.toString());
    }
}
/*2023-02-21 18:08:39.670  WARN 39518 --- [nio-8080-exec-1] c.b.i.dao.FopProductInventoryDao         : FopInventory - getErrorCode: 1381
2023-02-21 18:08:39.670  WARN 39518 --- [nio-8080-exec-1] c.b.i.dao.FopProductInventoryDao         : FopInventory - getErrorMessage: The Product Code you have entered is invalid.  Please enter a different Product Code.
2023-02-21 18:08:39.670  WARN 39518 --- [nio-8080-exec-1] c.b.i.dao.FopProductInventoryDao         : FopInventory - getErrorKey: orderentry.offercode.invalid
2023-02-21 18:08:39.670  WARN 39518 --- [nio-8080-exec-1] c.b.i.dao.FopProductInventoryDao         : FopInventory - getParameter: SkuOrKitCode*/
