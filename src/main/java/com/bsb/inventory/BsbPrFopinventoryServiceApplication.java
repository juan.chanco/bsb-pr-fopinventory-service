package com.bsb.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
    info = @Info(
        title = "FOP Inventory Service",
        description = "Store and serve the data exported from RS PIM",
        contact = @Contact(
            url = "https://bluestembrands.slack.com/archives/C01HBACPG1F",
            name = "PECO"
        )
    ),
    externalDocs = @ExternalDocumentation(),
    servers = {
       @Server(url = "/", description = "Default Server URL")
    }
)
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(value = {
        "com.bsb.common",
        "com.bsb.inventory",
        "com.bsb.fop",
})
public class BsbPrFopinventoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsbPrFopinventoryServiceApplication.class, args);
	}

}
