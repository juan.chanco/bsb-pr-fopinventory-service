package com.bsb.inventory.controller;

import com.bsb.fop.domain.Brand;
import com.bsb.fop.domain.CatalogCode;
import com.bsb.inventory.dao.FopProductInventoryDao;
import com.bsb.inventory.domain.PlatformInventory;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@RestController
public class InventoryController {
    private final FopProductInventoryDao fopProductInventoryDao;
    @GetMapping(value = "/brands/{brand}/skus/{sku}/catalogCodes/{catCode}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlatformInventory> get(
            @PathVariable Brand brand,
            @PathVariable String sku,
            @PathVariable CatalogCode catCode,
            @RequestParam(name = "cache", required = false, defaultValue = "false") boolean cache) {
            return ResponseEntity.of(fopProductInventoryDao.getInventory(sku, brand, catCode));
    }
	//@GetMapping(value = "/brands/{brand}/skus/{sku}/catalogCodes/{catCode}", produces = "application/json;charset=UTF-8")
	//public PlatformInventory get(
			//@PathVariable Brand brand,
			//@PathVariable String sku,
			//@PathVariable String catCode,
			//@RequestParam(name = "cache", required = false, defaultValue = "false") boolean cache) throws Exception {

		//LOGGER.debug("Get Single Sku - Sku: {}, Brand: {}, CatalogCode: {}, Cache: {}", brand, sku, catCode, cache);
		//long start = System.currentTimeMillis();

		//InventoryKey inventoryKey = new InventoryKey(sku, brand.name(), catCode);

		//PlatformInventory inventory = inventorySvc.getPlatformInventory(inventoryKey, cache);

		//long end = System.currentTimeMillis();
		//LOGGER.debug("Get Single Sku - Time: {} ms", end - start);

		//return inventory;
	//}

	//@DeleteMapping(value = "/cache/brands/{brand}/skus/{sku}/catalogCodes/{catCode}")
	//public void clearCache(
			//@PathVariable Brand brand,
			//@PathVariable String sku,
			//@PathVariable String catCode) {

		//LOGGER.debug("Clear Cache. Brand: {}, Sku: {}, CatCode: {}", brand, sku, catCode);
		//InventoryKey inventoryKey = new InventoryKey(sku, brand.name(), catCode);
		//inventorySvc.cacheEvict(inventoryKey);
	//}


	//@DeleteMapping(value = "/cache")
	//public void clearCache() {
		//inventorySvc.cacheEvict();
	//}


	//@ResponseStatus(HttpStatus.CREATED)
	//@PostMapping(value = "/brands/{brand}/catalogCodes/{catCode}",
			//consumes = "application/json;charset=UTF-8",
			//produces = "application/json;charset=UTF-8")
	//public List<PlatformInventory> multiSkuInventoryRetrievePost(
			//@PathVariable Brand brand,
			//@PathVariable String catCode,
			//@RequestBody List<String> skus,
			//@RequestParam(name = "cache", required = false, defaultValue = "false") boolean cache) throws Exception {

		//LOGGER.debug("Get Multiple Skus - Skus: {), Brand: {}, CatalogCode: {}, Cache: {}", skus, brand, catCode, cache);
		//long start = System.currentTimeMillis();

		//List<InventoryKey> inventoryKeys = skus.stream().map(sku -> new InventoryKey(sku, brand.name(), catCode)).collect(Collectors.toList());

		//List<PlatformInventory> inventories = inventorySvc.getPlatformInventoryList(inventoryKeys, cache);

		//long end = System.currentTimeMillis();
		//LOGGER.debug("Get Multiple Skus - Time: {} ms", end - start);

		//return inventories;

	//}

	//@PostMapping(value = "emit/brands/{brand}/catalogCodes/{catCode}")
	//public List<PlatformInventory> emit(
			//@PathVariable Brand brand,
			//@PathVariable String catCode,
			//@RequestBody List<String> skus) {
		//return inventorySvc.emit(brand, catCode, skus);
	//}

	//@ResponseStatus(HttpStatus.CREATED)
	//@PostMapping(value = "/mock/brands/{brand}/catalogCodes/{catCode}", consumes = "application/json;charset=UTF-8", produces = "application/json;charset=UTF-8")
	//public void mockInventoriesPost(
			//@PathVariable Brand brand,
			//@PathVariable String catCode,
			//@RequestBody List<PlatformInventory> inventories) throws Exception {
		//for (PlatformInventory inventory : inventories) {
			//LOGGER.info("Mock Inventories - Brand: {}, CatalogCode: {}, Sku: {], Count: {}", brand, catCode, inventory.getSKU(),
					//inventory.getInStockQuantity());
		//}

		//inventorySvc.saveMockInventories(inventories, brand, catCode);
	//}
}
