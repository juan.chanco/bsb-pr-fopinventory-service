package com.bsb.fop.config;

import com.bsb.fop.client.ProductInventoryClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ProductServiceConfig {
    @Value("${com.bsb.inventory.fop.baseUrl}")
    private String baseUrl;
    @Value("${com.bsb.inventory.fop.productSvc:Product.svc}")
    private String productSvc;
    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
         //this must match the package specified in wdsl java generation
        marshaller.setContextPath("com.bsb.fop.wsdl.product");
        return marshaller;
    }

    @Bean
    public ProductInventoryClient productInventoryClient(Jaxb2Marshaller marshaller) {
        ProductInventoryClient client = new ProductInventoryClient();
        client.setDefaultUri(baseUrl + productSvc);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
