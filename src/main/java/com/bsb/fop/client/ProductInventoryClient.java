package com.bsb.fop.client;

import com.bsb.fop.domain.Brand;
import com.bsb.fop.domain.CatalogCode;
import com.bsb.fop.wsdl.product.GetProductInventory;
import com.bsb.fop.wsdl.product.GetProductInventoryResponse;
import com.bsb.fop.wsdl.product.ObjectFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductInventoryClient extends WebServiceGatewaySupport {
    private static final ObjectFactory objectFactory = new ObjectFactory();

    public GetProductInventoryResponse getProductInventory(String sku, Brand brand, CatalogCode catalogCode) {
        return getProductInventory(sku, brand.toString(), catalogCode.toString());
    }

    public GetProductInventoryResponse getProductInventory(String sku, String brand, String catalogCode) {
        GetProductInventory request = objectFactory.createGetProductInventory();
        request.setSKU(objectFactory.createGetProductInventorySKU(sku));
        request.setBrandID(objectFactory.createGetProductInventoryBrandID(brand));
        request.setCatalogCode(objectFactory.createGetProductInventoryCatalogCode(catalogCode));

        log.info("Requesting inventory for {}/{}/{}", sku, brand, catalogCode);

        GetProductInventoryResponse response = (GetProductInventoryResponse) getWebServiceTemplate()
            .marshalSendAndReceive(request,
            //.marshalSendAndReceive("https://as.bluestembrands.com/Product.svc", request,
                    new SoapActionCallback(
                        "http://Fingerhut.Services/IProductSystem/GetProductInventory"));

        return response;
    }
}
