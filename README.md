# bsb-pr-fopinventory-service

Stores and serves up inventory data provided by FOP's SOAP interface. Notifies of changes via a kafka topic.
